﻿using System;
using InventarioPT.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace InventarioPT.Infraestructure.Data
{
    public partial class inventario_bdContext : DbContext
    {
        public inventario_bdContext()
        {
        }

        public inventario_bdContext(DbContextOptions<inventario_bdContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbAreas> TbAreas { get; set; }
        public virtual DbSet<TbEmpleados> TbEmpleados { get; set; }
        public virtual DbSet<TbInventario> TbInventario { get; set; }
        public virtual DbSet<TbInventarioareas> TbInventarioareas { get; set; }
        public virtual DbSet<TbInventarioempleados> TbInventarioempleados { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;port=3306;user=root;password=admin;database=inventario_bd", x => x.ServerVersion("8.0.21-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TbAreas>(entity =>
            {
                entity.ToTable("tb_areas");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.FechaUm)
                    .HasColumnName("FechaUM")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<TbEmpleados>(entity =>
            {
                entity.HasKey(e => e.Cedula)
                    .HasName("PRIMARY");

                entity.ToTable("tb_empleados");

                entity.Property(e => e.Cedula).ValueGeneratedNever();

                entity.Property(e => e.Apellidos)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Cargo)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.FechaUm)
                    .HasColumnName("FechaUM")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<TbInventario>(entity =>
            {
                entity.ToTable("tb_inventario");

                entity.Property(e => e.Descripcion)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.FechaCompra).HasColumnType("datetime");

                entity.Property(e => e.FechaUm)
                    .HasColumnName("FechaUM")
                    .HasColumnType("datetime");

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Serial)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Tipo)
                    .IsRequired()
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Valor).HasColumnType("decimal(10,0)");
            });

            modelBuilder.Entity<TbInventarioareas>(entity =>
            {
                entity.ToTable("tb_inventarioareas");

                entity.HasIndex(e => e.IdArea)
                    .HasName("fkid_inventario");

                entity.Property(e => e.FechaUm)
                    .HasColumnName("FechaUM")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdArea).HasColumnName("Id_Area");

                entity.Property(e => e.IdInventario).HasColumnName("Id_Inventario");

                entity.HasOne(d => d.IdAreaNavigation)
                    .WithMany(p => p.TbInventarioareas)
                    .HasForeignKey(d => d.IdArea)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fkid_inventario");
            });

            modelBuilder.Entity<TbInventarioempleados>(entity =>
            {
                entity.ToTable("tb_inventarioempleados");

                entity.HasIndex(e => e.IdPersona)
                    .HasName("fkid_inventario");

                entity.Property(e => e.FechaUm)
                    .HasColumnName("FechaUM")
                    .HasColumnType("datetime");

                entity.Property(e => e.IdInventario).HasColumnName("Id_Inventario");

                entity.Property(e => e.IdPersona).HasColumnName("Id_Persona");

                entity.HasOne(d => d.IdPersonaNavigation)
                    .WithMany(p => p.TbInventarioempleados)
                    .HasForeignKey(d => d.IdPersona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tb_inventarioempleado_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
