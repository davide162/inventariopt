﻿using InventarioPT.Core.DTOs;
using InventarioPT.Core.Entities;
using InventarioPT.Core.Interfaces;
using InventarioPT.Infraestructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventarioPT.Infraestructure.Repositories
{
    public class InventarioRepository : IInventarioRepository
    {
        private readonly inventario_bdContext _context;

        public InventarioRepository(inventario_bdContext context)
        {
            _context = context;
        }

        public async Task<bool> CreateInventario(InventarioDTO inventario)
        {
            if (inventario != null)
            {
                try
                {
                    TbInventario temp = new TbInventario
                    {
                        Nombre = inventario.Nombre,
                        Descripcion = inventario.Descripcion,
                        Estado = inventario.Estado,
                        FechaCompra = inventario.FechaCompra,
                        FechaUm = DateTime.Now,
                        Serial = inventario.Serial,
                        Tipo = inventario.Tipo,
                        Valor = inventario.Valor
                    };
                    _context.TbInventario.Add(temp);
                    _context.SaveChanges();
                    return true;
                }
                catch (Exception ex) { }
            }
            return false;
        }

        public async Task<bool> DeleteInventario(int id_inventario)
        {
            if (id_inventario > 0)
            {
                TbInventario temp = _context.TbInventario.FirstOrDefault(x => x.Id == id_inventario);
                if (temp != null)
                {
                    _context.TbInventario.Remove(temp);
                    _context.SaveChanges();
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> EditInventario(InventarioDTO inventario)
        {
            if (inventario != null)
            {
                if (inventario.Id > 0)
                {
                    TbInventario temp = _context.TbInventario.FirstOrDefault(x => x.Id == inventario.Id);
                    if (temp != null)
                    {
                        temp.FechaCompra = inventario.FechaCompra;
                        temp.Descripcion = inventario.Descripcion;
                        temp.Estado = inventario.Estado;
                        temp.Tipo = inventario.Tipo;
                        temp.Nombre = inventario.Nombre;
                        temp.Serial = inventario.Serial;
                        temp.Valor = inventario.Valor;
                        temp.FechaUm = DateTime.Now;
                        _context.SaveChanges();
                        return true;
                    }

                }
            }
            return false;
        }

        public async Task<IEnumerable<TbInventario>> GetInventarios()
        {
            var inventarios = await _context.TbInventario.ToListAsync();
            return inventarios;
        }

    }
}
