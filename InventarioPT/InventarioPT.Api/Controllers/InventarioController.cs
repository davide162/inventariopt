﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InventarioPT.Core.DTOs;
using InventarioPT.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InventarioPT.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventarioController : ControllerBase
    {
        private readonly IInventarioRepository _inventarioRepositorio;

        public InventarioController(IInventarioRepository inventarioRepository)
        {
            _inventarioRepositorio = inventarioRepository;
        }
        [HttpGet]
        public async Task<IActionResult> GetInventario()
        {
            var inventarios = await _inventarioRepositorio.GetInventarios();
            return Ok(inventarios);
        }

        [HttpPost]
        public async Task<IActionResult> CreateInventario(InventarioDTO inventario)
        {
            if(inventario != null)
            {
                await _inventarioRepositorio.CreateInventario(inventario);
                return Ok("Registro guardado exitosamente.");
            }
            return Ok("Ocurrio un error procesando la solicitud. Intentelo más tarde");
        }

        [HttpPut]
        public async Task<IActionResult> EditInventario(InventarioDTO inventario)
        {
            if (inventario != null)
            {
                await _inventarioRepositorio.EditInventario(inventario);
                return Ok("Registro editado exitosamente.");
            }
            return Ok("Ocurrio un error procesando la solicitud. Intentelo más tarde");
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteInventario(int id_inventario)
        {
            if (id_inventario >0)
            {
                await _inventarioRepositorio.DeleteInventario(id_inventario);
                return Ok("Registro guardado exitosamente.");
            }
            return Ok("Ocurrio un error procesando la solicitud. Intentelo más tarde");
        }
    }
}
