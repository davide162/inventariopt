/*
 Navicat Premium Data Transfer

 Source Server         : MYSQL_LOCAL
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : localhost:3306
 Source Schema         : inventario_bd

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 23/10/2020 00:19:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_areas
-- ----------------------------
DROP TABLE IF EXISTS `tb_areas`;
CREATE TABLE `tb_areas`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `FechaUM` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_areas
-- ----------------------------

-- ----------------------------
-- Table structure for tb_empleados
-- ----------------------------
DROP TABLE IF EXISTS `tb_empleados`;
CREATE TABLE `tb_empleados`  (
  `Cedula` int(0) NOT NULL,
  `Nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Apellidos` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Cargo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FechaUM` datetime(0) NOT NULL,
  PRIMARY KEY (`Cedula`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_empleados
-- ----------------------------

-- ----------------------------
-- Table structure for tb_inventario
-- ----------------------------
DROP TABLE IF EXISTS `tb_inventario`;
CREATE TABLE `tb_inventario`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Descripcion` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `Tipo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Serial` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Valor` decimal(10, 0) NOT NULL,
  `FechaCompra` datetime(0) NOT NULL,
  `Estado` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `FechaUM` datetime(0) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_inventario
-- ----------------------------
INSERT INTO `tb_inventario` VALUES (1, 'Asus mini', 'Descripcion de equipo, insumo o material', 'Oficina', '1234567asdfgh', 2500000, '2020-10-04 20:13:36', 'Activo', '2020-10-29 20:13:46');
INSERT INTO `tb_inventario` VALUES (2, 'prueba insumo', 'Descripcion de equipo, insumo o material', 'Oficina', 'ASD456888HHG', 345000, '2012-12-12 00:00:00', 'Activo', '2020-10-22 21:47:10');
INSERT INTO `tb_inventario` VALUES (4, 'Equipo prueba', 'Descripcion de equipo, insumo o material', 'Oficina', '9880LLLKJSHSG', 268000, '2012-12-12 00:00:00', 'Activo', '2020-10-22 21:47:10');
INSERT INTO `tb_inventario` VALUES (5, 'prueba Equipo', 'Descripcion de equipo, insumo o material', 'Oficina', '93JSJSJSKKK0001', 23600, '2012-12-12 00:00:00', 'Activo', '2020-10-22 21:47:10');
INSERT INTO `tb_inventario` VALUES (6, 'Prueba insumos1', 'Descripcion de equipo, insumo o material', 'Oficina', '1020LLKK000', 14800, '2012-12-12 00:00:00', 'Activo', '2020-10-22 21:47:10');
INSERT INTO `tb_inventario` VALUES (7, 'pruebas1', 'Descripcion de prueba', 'Pruebas', '1223qqqqassss', 123430, '2012-12-12 00:00:00', 'Activo', '2020-10-23 00:06:56');

-- ----------------------------
-- Table structure for tb_inventarioareas
-- ----------------------------
DROP TABLE IF EXISTS `tb_inventarioareas`;
CREATE TABLE `tb_inventarioareas`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `Id_Inventario` int(0) NOT NULL,
  `Id_Area` int(0) NOT NULL,
  `FechaUM` datetime(0) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fkid_inventario`(`Id_Area`) USING BTREE,
  INDEX `fk_id_inve`(`Id_Inventario`) USING BTREE,
  CONSTRAINT `fk_id_inve` FOREIGN KEY (`Id_Inventario`) REFERENCES `tb_inventario` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `fkid_inventario` FOREIGN KEY (`Id_Area`) REFERENCES `tb_areas` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_inventarioareas
-- ----------------------------

-- ----------------------------
-- Table structure for tb_inventarioempleados
-- ----------------------------
DROP TABLE IF EXISTS `tb_inventarioempleados`;
CREATE TABLE `tb_inventarioempleados`  (
  `Id` int(0) NOT NULL AUTO_INCREMENT,
  `Id_Inventario` int(0) NOT NULL,
  `Id_Persona` int(0) NOT NULL,
  `FechaUM` datetime(0) NOT NULL,
  PRIMARY KEY (`Id`) USING BTREE,
  INDEX `fkid_inventario`(`Id_Persona`) USING BTREE,
  INDEX `fk_id_inventa`(`Id_Inventario`) USING BTREE,
  CONSTRAINT `fk_id_inventa` FOREIGN KEY (`Id_Inventario`) REFERENCES `tb_inventario` (`Id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `tb_inventarioempleado_ibfk_1` FOREIGN KEY (`Id_Persona`) REFERENCES `tb_empleados` (`Cedula`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_inventarioempleados
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
