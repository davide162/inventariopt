﻿using InventarioPT.Core.DTOs;
using InventarioPT.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventarioPT.Core.Interfaces
{
    public interface IInventarioRepository
    {
        Task<IEnumerable<TbInventario>> GetInventarios();
        Task<bool> CreateInventario(InventarioDTO inventario);

        Task<bool> EditInventario(InventarioDTO inventario);
        Task<bool> DeleteInventario(int id_inventario);
    }
}
