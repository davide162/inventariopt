﻿using System;
using System.Collections.Generic;

namespace InventarioPT.Core.Entities
{
    public partial class TbInventario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }
        public string Serial { get; set; }
        public decimal Valor { get; set; }
        public DateTime FechaCompra { get; set; }
        public string Estado { get; set; }
        public DateTime FechaUm { get; set; }
    }
}
