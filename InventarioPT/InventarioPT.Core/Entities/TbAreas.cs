﻿using System;
using System.Collections.Generic;

namespace InventarioPT.Core.Entities
{
    public partial class TbAreas
    {
        public TbAreas()
        {
            TbInventarioareas = new HashSet<TbInventarioareas>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public DateTime? FechaUm { get; set; }

        public virtual ICollection<TbInventarioareas> TbInventarioareas { get; set; }
    }
}
