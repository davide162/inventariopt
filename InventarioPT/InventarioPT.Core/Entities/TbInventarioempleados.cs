﻿using System;
using System.Collections.Generic;

namespace InventarioPT.Core.Entities
{
    public partial class TbInventarioempleados
    {
        public int Id { get; set; }
        public int IdInventario { get; set; }
        public int IdPersona { get; set; }
        public DateTime FechaUm { get; set; }

        public virtual TbEmpleados IdPersonaNavigation { get; set; }
    }
}
