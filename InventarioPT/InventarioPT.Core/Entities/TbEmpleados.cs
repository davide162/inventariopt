﻿using System;
using System.Collections.Generic;

namespace InventarioPT.Core.Entities
{
    public partial class TbEmpleados
    {
        public TbEmpleados()
        {
            TbInventarioempleados = new HashSet<TbInventarioempleados>();
        }

        public int Cedula { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Cargo { get; set; }
        public DateTime FechaUm { get; set; }

        public virtual ICollection<TbInventarioempleados> TbInventarioempleados { get; set; }
    }
}
