﻿using System;
using System.Collections.Generic;

namespace InventarioPT.Core.Entities
{
    public partial class TbInventarioareas
    {
        public int Id { get; set; }
        public int IdInventario { get; set; }
        public int IdArea { get; set; }
        public DateTime FechaUm { get; set; }

        public virtual TbAreas IdAreaNavigation { get; set; }
    }
}
